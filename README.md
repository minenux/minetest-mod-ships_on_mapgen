# VenenuX minetest mod `ships_on_mapgen`

This is a mod for MineTest, named `ships_on_mapgen`

It spawns pre-built ships and rafts at mapgen time on water.

## Information

This mod spawns ships on oceans and lakes.

![screenshot.png](screenshot.png)

The largest one is the big pirate ship built (so it is rumored) by AspireMint. If you find one, 
explore it! But take care of pirates! (In small print: Pirates not included!)

In addition to the two capital ships, three types of rafts exist and may also show up on water. 
It is possible that rafts will lose the very tree trunks they mostly consist of; mapgen thinks 
they're trees. There is no good way to prevent that. Lucikly, most ships will survive intact 
and await explorers.

## Technical info

It does not depend on any particular mapgen.

#### Dependences

* handle_schematics.
* cottages
* beds
* doors
* default
* stairs

#### Optional Depends

* moreblocks
* stairsplus

#### How to use/install

This version can be downloaded from https://codeberg.org/minenux/minetest-mod-ships_on_mapgen/

The directory must be named `ships_on_mapgen` with all those files. of just grab from minetest interface.

#### Provability of ships spawn

In `init.lua` the firsts lines said:

`ships_on_mapgen.probability_per_mapchunk = 70;`

Is the percentage per mapchunk, with 100 will render at leas one ship on every sea or lake.

#### Added and hack more ships

You can of course add your own ships. Just edit `init.lua` and an appropriate entry in `ships_on_mapgen.schematics`. 
If you use an .mts file saved by handle_schematics, the mod can automaticly determine from the filename 
how deep the ship proceeds under water. Else set e.g. yoff=-2 if your ship continues 2 nodes below water. 

Example, added onemore line to as:

`{scm="name_of_file_in_schems_folder_without_extension", typ="ship", is_ship=true, yoff=-1, orients={0}}`

Grab the `.mts` file, save them in the schems/ directory and add an entry for them in the init.lua file.

## LICENCE

The ship pirate_ship_aspiremint_3_90 was created by AspireMint. License: CC-BY-SA 3.0

raft_1_2_180, raft_2_2_180, raft_3_2_180 and ship_sokomine_big_3_180 where created by
me, Sokomine, and are also released under CC-BY-SA 3.0

License of code: GPLv3.0
